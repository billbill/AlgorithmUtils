#!/usr/bin/python
#-*- encoding=utf-8 -*-
import sys
from collections import deque
reload(sys)
sys.setdefaultencoding("utf-8")

#BFS sub routine, print from target to bottom
def printtree_fromtarget(node,target,level):
    returnval=False
    if (target > level):
        for item in node.child:
            if(printtree_fromtarget(item,target,level+1)):
                returnval=True
    else:
        print("%c",node.m_data)
        if node.child is not None:
            returnval=True
    return returnval

#BFS routine, print from target to bottom
def printbfstree_fromtarget(root,target=0):
    print("---------------------------------------------")
    print ("Begin to print the tree from the specific level %c to bottom:"%str(target))
    if root is None:
        return
    while(printtree_fromtarget(root,target,0)):
        target=target+1
        print("------------------------")

#BFS sub routine, print from top to target
def printtree_totarget(node,target,reverse_level):
    returnval=False
    if (target < reverse_level):
        for item in node.child:
            if(printtree_totarget(item,target,reverse_level-1)):
                returnval=True
    else:
        print("%c",node.m_data)
        if node.child is not None:
            if (reverse_level == 0):
                returnval=False
            else:
                returnval=True
    return returnval

#BFS routine, print from top to target
def printbfstree_totarget(root,target):
    print("---------------------------------------------")
    print("Begin to print the tree from top to the specific level %c:"%str(target))
    if root is None:
        return
    reverse_level = target
    while(printtree_totarget(root,target,reverse_level)):
        target = target-1
        print("------------------------")

#BFS sub routine, print the specific target
def printtree_target(node,target,level):
    returnval=False
    if (target > level):
        for item in node.child:
            if(printtree_fromtarget(item,target,level+1)):
                returnval=True
    else:
        print("%c",node.m_data)
        if node.child is not None:
            returnval=True
    return returnval

#BFS routine, print the specific target
def printbfstree_target(root,target=0):
    print("---------------------------------------------")
    print("Begin to print the specific level %c of tree:"%str(target))
    if root is None:
        return
    while(printtree_target(root,target,0)):
        print("------------------------")
        break

class BinaryTreeNode():
    def __init__(self,m_data="",lchild=None,rchild=None):
        self.m_data = m_data
        self.lchild = lchild
        self.rchild = rchild
        self.child = []

class BinaryTree():
    def __init__(self,array=[]):
        self.binaryNodeQue = deque([])
        self.root = self.makeBinaryTree(array)

    def makeBinaryTree(self,array=[]):
        root = None
        if(len(array)>0):
            root = BinaryTreeNode(str(array[0]))
            self.binaryNodeQue.appendleft(root)
            iterator = 1
            #while((len(self.binaryNodeQue)>0) and (iterator<len(array))):
            while(iterator<len(array)):
                node = self.binaryNodeQue.pop()
                lhs = BinaryTreeNode(str(array[iterator]))
                node.lchild = lhs
                node.child.append(node.lchild)
                self.binaryNodeQue.appendleft(node.lchild)
                iterator = iterator+1
                if(iterator<len(array)):
                    rhs = BinaryTreeNode(str(array[iterator]))
                    node.rchild = rhs
                    node.child.append(node.rchild)
                    self.binaryNodeQue.appendleft(node.rchild)
                    iterator = iterator+1
        return root

    def preOrder(self):
        print("preOrder:")
        self.m_preOrder(self.root)

    def m_preOrder(self,root=None):
        if root is None:
            pass
        else:
            print str(root.m_data)
            self.m_preOrder(root.lchild)
            self.m_preOrder(root.rchild)

    def midOrder(self):
        print("midOrder:\n")
        self.m_midOrder(self.root)

    def m_midOrder(self,root=None):
        if root is None:
            pass
        else:
            self.m_midOrder(root.lchild)
            print str(root.m_data)
            self.m_midOrder(root.rchild)

    def postOrder(self):
        print("postOrder:\n")
        self.postOrder(self.root)

    def m_postOrder(self,root=None):
        if root is None:
            pass
        else:
            self.m_postOrder(root.lchild)
            self.m_postOrder(root.rchild)
            print str(root.m_data)

    def destroyBinaryTree(self):
        self.destroy(self.root)

    def destroy(self):
        tmpBinaryTreeNode = self.root
        if (tmpBinaryTreeNode is None):
            pass
        else:
            self.destroy(tmpBinaryTreeNode.lchild)
            self.destroy(tmpBinaryTreeNode.rchild)
        tmpBinaryTreeNode = None

if __name__ == "__main__":
    tree_data = []
    for i in range(10):
        tree_data.append(i)
    tree = BinaryTree(tree_data)
    #tree.preOrder()
    #tree.midOrder()
    printbfstree_fromtarget(tree.root,2)
    printbfstree_totarget(tree.root,1)
    printbfstree_target(tree.root,3)
